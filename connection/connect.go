package connection

import(	
	"database/sql"  
	_"github.com/go-sql-driver/mysql"  
    "log"
)

func Connection() *sql.DB{
	db, err := sql.Open("mysql", "root:finnet10@@tcp(172.18.0.7:3306)/painelfornecedormultibanco")
	
	if err != nil {
		log.Fatal("Error opening connection with database: ", err)
		defer db.Close()
	}

	return db
}