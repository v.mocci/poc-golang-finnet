package main

import(	
	"fmt"
	"net/http"
	"log"
	_"github.com/go-sql-driver/mysql"	
	hubEndpoints "poc-golang-finnet/app/endpoints/hubs"	
	mongoEndpoints "poc-golang-finnet/app/endpoints/mongoDb"	
	notasEndpoints "poc-golang-finnet/app/endpoints/notaDetalhe"
	contaEndpoints "poc-golang-finnet/app/endpoints/conta"		
)

func main(){
	handleRequests()
}

func handleRequests(){
	http.HandleFunc("/", homePage)	
	http.HandleFunc("/insertUserOnMongo", mongoEndpoints.InsertUserWithMongo)
	http.HandleFunc("/returnAllNotaDetalhe", notasEndpoints.ReturnAllNotaDetalhe)
	http.HandleFunc("/returnAllHubs", hubEndpoints.ReturnAllHubs)
	http.HandleFunc("/returnContas", contaEndpoints.ReturnContaAsJson)
	log.Fatal(http.ListenAndServe(":8089", nil))
}

func homePage(w http.ResponseWriter, r *http.Request){	
   fmt.Fprintf(w,
   " <p>Available routes (only GET requisitions): <br><br> /insertUserOnMongo <br> /returnAllNotaDetalhe <br> /returnAllHubs <br> /returnContas")	
}