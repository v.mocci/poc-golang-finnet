# Poc Golang Finnet

Simple system/API with Golang, MySQL and MongoDB technologies for queries and database consulting.

# TO DO

- [:x:] Use GORM for database transactions: http://learningprogramming.net/golang/gorm/select-in-gorm/ 
- [:heavy_check_mark:] Return JSON data from endpoints 

# New Functionalities

- JSON return on Conta table (localhost:8089/returnContas)