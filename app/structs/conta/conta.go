package StructsConta

import("database/sql")

type Conta struct{
	ID int
	AGENCIA_ID string
	CONTA_CODIGO string
	CONTA_CODIGO_DIGITO sql.NullString
	CONTA_QUEM sql.NullInt64
	CONTA_QUANDO  string	
	CONTA_FLAG_ATIVO int
}
