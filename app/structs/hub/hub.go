package StructsHub

import("database/sql")

type Hub struct{
	ID int
	CNPJ string
	CODIGO string
	APELIDO sql.NullString
	DESCRICAO string
	QUEM  sql.NullInt64
	QUANDO string
	FLAG_ATIVO int
}
