package Conta

import(	
	"fmt"
	"time"
	"net/http"
	"encoding/json"  	
	"log"	
	conn "poc-golang-finnet/connection"
   _"github.com/go-sql-driver/mysql"	
	contaStructs "poc-golang-finnet/app/structs/conta"
)

var connection = conn.Connection()

func ReturnContaAsJson(w http.ResponseWriter, r *http.Request){
	transactionBeggining := time.Now()

	query, err := connection.Query("SELECT * FROM conta WHERE cta_id = 2")	

		if err != nil {
			log.Fatal(err)
		}

		for query.Next(){
			var conta contaStructs.Conta
			err := query.Scan(
				&conta.ID, 
				&conta.AGENCIA_ID,
				&conta.CONTA_CODIGO,
				&conta.CONTA_CODIGO_DIGITO,
				&conta.CONTA_QUEM,			
				&conta.CONTA_QUANDO,
				&conta.CONTA_FLAG_ATIVO)

			if err != nil {
				log.Fatal(err)
			}

			jsonResponse, err := json.Marshal(conta)			
			w.Write(jsonResponse)
		}	

	verifyTransactionExecutionTime := time.Since(transactionBeggining)
		fmt.Println("The operation returned in: ", verifyTransactionExecutionTime.Seconds(), " seconds")
}