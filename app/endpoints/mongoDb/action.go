package MongoDb

import(	
	"fmt"
	"net/http"
	"log"	
	"context"	
	"time"	
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"		
   _"github.com/go-sql-driver/mysql"	
	userStructs "poc-golang-finnet/app/structs/user"		
)

//var connection = conn.Connection()

//@TODO: rewrite to app/endpoints -- Vinicius Mocci --
func InsertUserWithMongo(w http.ResponseWriter, r *http.Request){	

	//Mongo Connection
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil { 
		log.Fatal(err) 
		fmt.Println("Mongo connect error: ", err, client)
	}

	//Insert User
	
	//user := User{ID: 2, Name: "Mocci", Email: "vinicius.smocci@gmail.com", Type:"admin"}
	user := userStructs.User{ID: 3, Name: "Vini Mocci", Email: "vinicius.mocci@yahoo.com.br", Type:"master"}
	//user := User{ID: 4, Name: "Vini", Email: "vinicius@gmail.com", Type:"fornecedor"} 

	collection := client.Database("GolangDatabase").Collection("user")

	insertResult, err := collection.InsertOne(context.TODO(), user)

	if err !=nil {
		log.Fatal(err)
	}

	fmt.Println("Inserted user: ", insertResult.InsertedID)
	
}

