package Hubs

import(	
	"fmt"
	"net/http"
	"log"			
	"time"		
	conn "poc-golang-finnet/connection"
   _"github.com/go-sql-driver/mysql"	
	hubStructs "poc-golang-finnet/app/structs/hub"		
)

var connection = conn.Connection()

func ReturnAllHubs(w http.ResponseWriter, r *http.Request){
	
	transactionBeggining := time.Now()		
	query, err := connection.Query("SELECT * FROM hub")	

	if err != nil {
		log.Fatal(err)
	}

	for query.Next(){
		var hub hubStructs.Hub
		err := query.Scan(&hub.ID, &hub.CNPJ, &hub.CODIGO, &hub.APELIDO, &hub.DESCRICAO, &hub.QUEM, &hub.QUANDO, &hub.FLAG_ATIVO)

		if err != nil {
			log.Fatal(err)
		}

		fmt.Fprintf(w, "%s\n", hub.CNPJ)		
	}	

	verifyTransactionExecutionTime := time.Since(transactionBeggining)
		fmt.Println("The operation returned in: ", verifyTransactionExecutionTime.Seconds(), " seconds")
	
}