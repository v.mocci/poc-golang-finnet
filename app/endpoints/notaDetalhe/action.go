package NotaDetalhe

import(	
	"fmt"
	"net/http"
	"log"			
	"time"		
	conn "poc-golang-finnet/connection"
   _"github.com/go-sql-driver/mysql"	
   notaDetalheStructs "poc-golang-finnet/app/structs/notaDetalhe"			
)

var connection = conn.Connection()

func ReturnAllNotaDetalhe(w http.ResponseWriter, r *http.Request){

	transactionBeggining := time.Now()	

	res, err := connection.Query("SELECT * FROM nota_detalhe ntd")
	
	// To close connection use:
	//defer connection.Close()

	if err != nil {
		log.Fatal(err)
	}

	for res.Next(){
		var nota notaDetalheStructs.SelectAllNotaDetalhe
		err := res.Scan(
			&nota.Id,
			&nota.Ntl_Id,
			&nota.Nts_Id,
			&nota.Ara_Codigo,
			&nota.For_Id,
			&nota.Det_Id,
			&nota.Opa_Id,
			&nota.Tpm_Id,
			&nota.Ntd_Emp_Dest_Id,
			&nota.Ntb_Id,
			&nota.Ntd_Banco,
			&nota.Ntd_Agencia,
			&nota.Ntd_Digito_Agencia,
			&nota.Ntd_Conta,
			&nota.Ntd_Digito_Conta,
			&nota.Ntd_Digito_Conta_Agencia,
			&nota.Ntd_Det_Num_Docto,
			&nota.Ntd_Det_Num_Complementar,
			&nota.Ntd_Forma_Lancamento,
			&nota.Ntd_Serie_Nf,
			&nota.Ntd_Numero_Nf,
			&nota.Ntd_Vencimento,
			&nota.Ntd_Valor_Bruto,
			&nota.Ntd_Valor_Liquido,
			&nota.Ntd_Data_Emissao,
			&nota.Ntd_Total_Parcela,
			&nota.Ntd_Numero_Parcela,
			&nota.Ntd_Descricao,
			&nota.Ntd_Documento_Baixa,
			&nota.Ntd_Arquivo_Anexo,
			&nota.Ntd_Chave_Xml,
			&nota.Ntd_Data_Baixa,
			&nota.Ntd_Conta_Contabil,
			&nota.Ntd_Motivo,
			&nota.Ntd_Projeto,
			&nota.Ntd_Centro_Custo,
			&nota.Ntd_Ordem_Interna,
			&nota.Ntd_Valor_Lancamento,
			&nota.Ntd_Ir_Pf_Pj,
			&nota.Ntd_Cofins,
			&nota.Ntd_Pis,
			&nota.Ntd_Csll,
			&nota.Ntd_Icms,
			&nota.Ntd_Ipi,
			&nota.Ntd_Ii,
			&nota.Ntd_Ie,
			&nota.Ntd_Iof,
			&nota.Ntd_Itr,
			&nota.Ntd_Cide,
			&nota.Ntd_Inss,
			&nota.Ntd_Iss,
			&nota.Ntd_Irrf,
			&nota.Ntd_Irf,
			&nota.Ntd_Ret,
			&nota.Ntd_Qem,
			&nota.Ntd_Qdo,
			&nota.Ntd_Flg_Atv,
			&nota.Ntd_Input_Date,
			&nota.Ntd_Last_Update,
			&nota.Ntd_Banco_Proprio_Flg_Atv,
			&nota.Ntd_Origem_Carga,
			&nota.Ntd_Agenda,
			&nota.Ntd_Data_Pagamento)

			if err != nil {
				log.Fatal(err)
			}	
			
			fmt.Fprintf(w, "%v\n", nota.Id)
	}

	verifyTransactionExecutionTime := time.Since(transactionBeggining)
		fmt.Println("The operation returned in: ", verifyTransactionExecutionTime.Milliseconds(), " miliseconds", " // ", "(Equivalent to: ", verifyTransactionExecutionTime.Seconds(), " seconds)")
	
}